import { Palette } from './types'

export const palette: Palette = {
  primary: [
    '#e4ffee', // 50
    '#beffd5', // 100
    '#8bffb9', // 200
    '#33ff99', // 300 primary
    '#00fb7d', // 400
    '#00f568', // 500
    '#00e35c', // 600
    '#00ce4e', // 700
    '#00bc42', // 800
    '#00992c', // 900
  ],
  secondary: [
    '#dff7ff', // 50
    '#ace9fe', // 100
    '#70dbfe', // 200
    '#00ccff', // 300 secondary
    '#00c0ff', // 400
    '#00b4ff', // 500
    '#00a6f4', // 600
    '#0093e1', // 700
    '#0081cd', // 800
    '#0061ac', // 900
  ],
  gray: [
    '#f7f7f7', // 50,
    '#eeeeee', // 100,
    '#e2e2e2', // 200,
    '#d0d0d0', // 300,
    '#ababab', // 400,
    '#8a8a8a', // 500,
    '#636363', // 600,
    '#505050', // 700,
    '#323232', // 800,
    '#121212', // 900,
  ],
  dark: [
    '#F3F4F4', // 50 off-white
    '#d1d5da', // 100 light grey
    '#b4b9c2', // 200
    '#969dab', // 300 medium grey
    '#a9b3be', // 400
    '#89939d', // 500
    '#616b75', // 600 grey
    '#4e5761', // 700
    '#303942', // 800 dark grey
    '#101921', // 900 night black
  ],
}

const { secondary, primary } = palette

export const BIFROST_BLUE = secondary[3]
export const YGGDRASIL_GREEN = primary[3]
export const MIDGARD_TURQUOISE = '#23DCC8'
export const FLASH_ORANGE = '#F3BA2F'
export const SURTR_RED = '#FF4954'
