# `@thorchain/asgardex-binance`

## Modules

- `client` - Custom client for communicating with Binance Chain by using [`binance-chain/javascript-sdk`](https://github.com/binance-chain/javascript-sdk)
- `types` - TypeScript type definitions for [`binance-chain/javascript-sdk`](https://github.com/binance-chain/javascript-sdk) (not completed).
- `util` - Utitilies for using [`binance-chain/javascript-sdk`](https://github.com/binance-chain/javascript-sdk)

## Installation

```
yarn add @thorchain/asgardex-binance
```

Following peer dependencies have to be installed into your project. These are not included in `@thorchain/asgardex-binance`.

```
yarn add @binance-chain/javascript-sdk
```
